package net.jebu.sample;

import java.io.IOException;

public interface IDiskBackedCollection<T> {
    // collection methods
    T get(int index);
    void set(int index, T val) throws IOException;
    void delete(int index) throws IOException;
    void push(T val) throws IOException;
    T pop() throws IOException;

    // memory tune parameters
    int getMemorySize();
    void setMemorySize(int size);

    // disk tune parameters
    int getDiskSize();
    void setDiskSize(int size);
}
