package net.jebu.sample.sequence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TemporarySpaceManager<T> {
    Path tempPath;

    // current size of the sequence
    int size = 0;

    // currently loaded start offset
    int offset = 0;
    // size of currently loaded segment
    int segmentSize = 0;
    // max in memory size
    int maxSegmentSize = 0;

    // in memory block of values
    Object[] memArray;

    // offset map
    List<SequenceFileValue<T>> offsetMap = new ArrayList<>();


    public TemporarySpaceManager(String prefix, int size) throws IOException {
         tempPath = Files.createTempDirectory(prefix);
         memArray = new Object[size];
         maxSegmentSize = size;
    }

    public void writeValue(SequenceFileValue sequenceFileValue) throws IOException {
        Path filePath = sequenceFileValue.current;
        if (filePath == null) {
            String filename = UUID.randomUUID().toString();
            // TODO: figure out relative
            filePath = Files.createFile(tempPath);
        }
        SequenceFileValue.pickle(filePath, sequenceFileValue);
    }

    public SequenceFileValue readValue(Path filename) throws IOException, ClassNotFoundException {
        // TODO: figure out relative
        return SequenceFileValue.unpickle(filename);
    }

    protected void push(T val) throws IOException {
        if (offset != 0) {
            swapIn(0);
        }

        SequenceFileValue<T> firstElement = (SequenceFileValue<T>) memArray[0];
        SequenceFileValue newElement = new SequenceFileValue(val);
        newElement.setNext(firstElement.current);
        newElement.setPrevious(null);
        writeValue(newElement);

        firstElement.setPrevious(newElement.current);
        writeValue(firstElement);

        if (segmentSize >= maxSegmentSize) {
            // manipulate all offsets and push
            for (int i=0; i<offsetMap.size(); i++) {
                SequenceFileValue sfv = offsetMap.get(i);
                try {
                    offsetMap.set(i, readValue(sfv.previous));
                }
                catch (ClassNotFoundException c) {
                    System.out.println(c);
                }
            }

            if (((size + 1) % maxSegmentSize) == 1) {
                // new element has to be inserted size we have crossed the memory size limit
                SequenceFileValue sfv = offsetMap.get(offsetMap.size() - 1);
                try {
                    while (sfv.next != null) {
                        sfv = readValue(sfv.next);
                    }
                    offsetMap.add(sfv);
                }
                catch (ClassNotFoundException c) {
                    System.out.println(c);
                }
            }
        } else {
            // increment segment size
            segmentSize++;
        }

        // now we have a push into the memory only
        for (int i=segmentSize; i>0; i-- ) {
            memArray[i] = memArray[i-1];
        }
        memArray[0] = newElement;

    }

    protected SequenceFileValue<T> get(int index) {
        checkAndLoadFromDisk(index);
        int seekOffset = index % maxSegmentSize;
        return (SequenceFileValue<T>) memArray[seekOffset];
    }

    protected void set(int index, T val) throws IOException {
        checkAndLoadFromDisk(index);
        int seekOffset = index % maxSegmentSize;
        SequenceFileValue sfv = (SequenceFileValue<T>) memArray[seekOffset];
        sfv.value = val;
        writeValue(sfv);
    }

    protected void delete(int index) throws IOException{
        checkAndLoadFromDisk(index);
        int seekOffset = index % maxSegmentSize;
        SequenceFileValue sfv = (SequenceFileValue<T>) memArray[seekOffset];

        // move around the pointers
        if (sfv.next != null) {
            SequenceFileValue nextVal = null;
            try {
                nextVal = readValue(sfv.next);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            nextVal.previous = sfv.previous;
            writeValue(nextVal);
        }

        if (sfv.previous != null) {
            SequenceFileValue prevVal = null;
            try {
                prevVal = readValue(sfv.previous);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            prevVal.next = sfv.next;
            writeValue(prevVal);
        }

        // remove it from the in memory array
        int i = seekOffset;
        for (; i < (segmentSize-1) && memArray[i] != null; i++) {
            memArray[i] = memArray[i+1];
        }

        //adjust all further indexes to point to the next one in the list
        if (i == (segmentSize-1)) {
            SequenceFileValue lastVal = (SequenceFileValue<T>) memArray[i];
            if (lastVal.next != null) {
                try {
                    memArray[i] = readValue(lastVal.next);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                // we now need to move the index offsets starting from the current one
                for (int j=offset+1; j < offsetMap.size(); j++) {
                    SequenceFileValue nextIndexVal = offsetMap.get(j);
                    if (nextIndexVal.next != null) {
                        try {
                            offsetMap.set(j, (SequenceFileValue<T>) readValue(nextIndexVal.next));
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        offsetMap.remove(j);
                    }
                }
            } else {
                segmentSize--;
            }
        }
        size--;

    }

    // private methods
    private void checkAndLoadFromDisk(int index) {
        if (index > size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int seekIndex = index / maxSegmentSize;
        if (seekIndex != offset) {
            swapIn(seekIndex);
        }

    }
    private void swapIn(int index) {
        SequenceFileValue sfv = offsetMap.get(index);
        offset = index;
        segmentSize = 0;
        for (int i=0; i < maxSegmentSize; i++) {
            memArray[i] = sfv;
            if (sfv != null){
                segmentSize++;
                if (sfv.next != null) {
                    try {
                        sfv = readValue(sfv.next);
                    }
                    catch (IOException e) {
                        System.out.println(e);
                        sfv = null;
                    }
                    catch (ClassNotFoundException c) {
                        System.out.println(c);
                        sfv = null;
                    }
                } else {
                    sfv = null;
                }
            }
        }
    }
}
