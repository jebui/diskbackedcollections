package net.jebu.sample.sequence;

import net.jebu.sample.IDiskBackedCollection;

import java.io.IOException;

public class DiskBackedSequence<T> implements IDiskBackedCollection<T> {
    private TemporarySpaceManager<T> temporarySpaceManager;

    public DiskBackedSequence(int memorySize) throws IOException {
        temporarySpaceManager = new TemporarySpaceManager("disq", memorySize);
    }

    @Override
    public T get(int index) {
        return temporarySpaceManager.get(index).value;
    }

    @Override
    public void set(int index, T val) throws IOException{
        temporarySpaceManager.set(index, val);
    }

    @Override
    public void delete(int index) throws IOException{
        temporarySpaceManager.delete(index);
    }

    @Override
    public void push(T val) throws IOException {
        temporarySpaceManager.push(val);

    }

    @Override
    public T pop() throws IOException {
        T val = get(0);
        temporarySpaceManager.delete(0);
        return val;
    }

    @Override
    public int getMemorySize() {
        return 0;
    }

    @Override
    public void setMemorySize(int size) {

    }

    @Override
    public int getDiskSize() {
        return 0;
    }

    @Override
    public void setDiskSize(int size) {

    }
}
