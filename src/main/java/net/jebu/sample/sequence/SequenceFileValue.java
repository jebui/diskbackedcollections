package net.jebu.sample.sequence;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class SequenceFileValue<T> implements Serializable {
    Path previous, next, current;
    T value;

    public SequenceFileValue(T value) {
        this.value = value;
        this.previous = null;
        this.next = null;
        this.current = null;
    }

    public void setPrevious(Path previous) {
        this.previous = previous;
    }

    public void setNext(Path next) {
        this.next = next;
    }

    public static void pickle(Path filename, SequenceFileValue seqObj) throws IOException {
        if (seqObj.current == null) {
            seqObj.current = filename;
        }
        OutputStream fos = Files.newOutputStream(filename);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(seqObj);
        oos.close();
        fos.close();
    }

    public static SequenceFileValue unpickle(Path filename) throws IOException, ClassNotFoundException {
        InputStream fis = Files.newInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(fis);
        SequenceFileValue sfv = (SequenceFileValue) ois.readObject();
        ois.close();
        fis.close();
        return sfv;
    }

}
