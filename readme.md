# Approach
* Values stored on disk in a doubly linked list file structure
* In memory index of offsets
* load relevant chunk into memory
* write thru any operation into disk
* uses Java serialization to persist to disk
* file paths are stored for list pointers

# Optimisations
* RocksDB for storage, persist the object into RocksDB with the UUID as key